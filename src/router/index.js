import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'
import Login from '@/views/Login'
import AddTest from '@/views/home/AddTest'
import LookTest from "../views/home/LookTest";
import Listtest from "../views/home/ListTest.vue";
import ClassManage from "../views/home/ClassManage";
import ClassRoomManage from "../views/home/ClassRoomManage";
import StudentManage from "../views/home/StudentManage";

import PendingClass from '../views/home/class/PendingClass.vue';
import AddExam from "../views/home/exam/AddExam.vue";
import ListExam from "../views/home/exam/ListExam.vue";
import AddUser from "../views/home/user/AddUser.vue";
import ShowUser from "../views/home/user/ShowUser.vue"
import Cookies from "js-cookie";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path:'/home',
            name:'home',
            component:Home,
            children:[
                {
                    path:'/home/addtest',
                    name:'addtest',
                    component:AddTest
                },
                {
                    path:'/home/looktest',
                    name:'looktest',
                    component:LookTest
                },
                {
                    path:'/home/listtest',
                    name:'listtest',
                    component:Listtest
                },
                {
                    path:'/home/classmanage',
                    name:'classmanage',
                    component:ClassManage
                },
                {
                    path:'/home/classroommanage',
                    name:'classroommanage',
                    component:ClassRoomManage
                },
                {
                    path:'/home/studentmanage',
                    name:'studentmanage',
                    component:StudentManage
                },
                {
                    path:'/home/class/pendingclass',
                    name:'pendingclass',
                    component:PendingClass
                },
                {
                    path:'/home/exam/addexam',
                    name:'addexam',
                    component:AddExam
                },
                {
                    path:'/home/exam/listexam',
                    name:'listexam',
                    component:ListExam
                },
                {
                    path:'/home/user/showuser',
                    name:'showuser',
                    component:ShowUser
                },
                {
                    path:'/home/user/adduser',
                    name:'adduser',
                    component:AddUser
                }
            ]
        },
        {
            path:'/login',
            name:'login',
            component:Login
        },
        {
            path:'/',
            redirect:'/login'
        }
    ]
})

router.beforeEach((to, from, next) => {
    console.log(Cookies.get('token'), 'token');
    if (to.path !== '/login') {
        if(Cookies.get('token')) {
            next()
        }else {
            next('/login')
        }
    }else {
        next()
    }
})
export default router;
