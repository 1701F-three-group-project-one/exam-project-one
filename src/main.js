// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

Vue.use(mavonEditor);

import http from './utils/http'
Vue.prototype.$axios = http;

Vue.prototype.$bus = new Vue();

//全部班级
Vue.prototype.getGrade = function (api) {
    this.$axios.get(api).then(({data})=>{
        this.gradeData = data.data
    }).catch((error)=>{
        console.log(error)
    })
}
//全部教室
Vue.prototype.getRoom = function (api) {
    this.$axios.get(api).then(({data})=>{
        this.room = data.data
    }).catch((error)=>{
        console.log(error)
    })
}

Vue.config.productionTip = false;
Vue.use(ElementUI);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
});
